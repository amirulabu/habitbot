# habitbot

Telegram bot for tracking your daily habits. CS50 2019 Final Project. 

Demo at [@MyHabitBot](http://telegram.me/myhabitbot)

Build using `Django`

# Installation

* Create a new postgresql database and user 
* Create and fill in `.env` file
* `pip install -r requirements.txt`
* `python manage.py migrate`
* `python manage.py createsuperuser`
* Deploy django app as per instructions [here](http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/)
* Set cron for `python manage.py trigger_habits` every hour.
* Set cron for `python manage.py reset_state` every hour + minutes you want to give the user chance to write a note before the state resets(I would recommend 10 minutes)


# Development setup


* Create a new postgresql database and user 
* Install [ngrok](https://ngrok.com/)
* Create and fill in `.env` file
* `pip install -r requirements.txt`
* `python manage.py migrate`
* `python manage.py createsuperuser`
* `python manage.py runserver`
* `ngrok http 8000`     
* `python manage.py set_webhook <ngrok link>`
* create an `.env` file at the same level as manage.py and fill in the details below

```
export PG_DB_NAME=
export PG_USER=
export PG_PASS=
export TG_BOT=
export TG_USER_ID=
export SECRET_KEY=
export CURRENT_DOMAIN=
export TELEGRAM_DEBUG=
```
