from django.contrib import admin
from django.urls import path, include
from django.http import HttpResponse, HttpResponseRedirect

urlpatterns = [
    path("", include("bot.urls")),
    path("admin/", admin.site.urls),
    path("account/", include("accounts.urls")),
    path(
        "robots.txt",
        lambda x: HttpResponse(
            "User-Agent: *\nDisallow: /bot/", content_type="text/plain"
        ),
        name="robots_file",
    ),
]
