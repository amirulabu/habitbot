from django.contrib import admin
from .models import Profile, Habit, Reply


class ReplyAdmin(admin.ModelAdmin):
    readonly_fields = ("created_at", "last_modified_at")
    list_display = ("id", "user", "habit", "done", "note", "created_at")


class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        "telegram_id",
        "username",
        "first_name",
        "last_name",
        "state",
        "created_at",
    )


class HabitAdmin(admin.ModelAdmin):
    list_display = ("user", "time", "name")


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Habit, HabitAdmin)
admin.site.register(Reply, ReplyAdmin)
