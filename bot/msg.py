import requests
from .helper import get_url


class Message:
    """ Manage message from the telegram user """

    def __init__(self, update):
        self.update = update
        self.message = update["message"]
        self.text = update["message"]["text"]
        self.chat_id = update["message"]["from"]["id"]
        self.first_name = update["message"]["from"]["first_name"]
        # Telegram allow accounts without last name
        self.last_name = (
            update["message"]["from"]["last_name"]
            if update["message"]["from"].get("last_name")
            else None
        )
        # Telegram allow accounts without username
        self.username = (
            update["message"]["from"]["username"]
            if update["message"]["from"].get("username")
            else None
        )
        self.is_bot = update["message"]["from"]["is_bot"]
        self.date = update["message"]["date"]

    def full_name(self):
        return (
            "{} {}".format(self.first_name, self.last_name)
            if self.last_name
            else "{}".format(self.first_name)
        )

    def name_or_username(self):
        return self.username if self.username else self.first_name

    def reply_message(
        self, reply_text="I can hear you!", keyboard=None, inline_keyboard=None
    ):
        """
            Reply to the sender

                :param reply_text: text to reply
                :param keyboard: refer ReplyKeyboardMarkup type 
            https://core.telegram.org/bots/api/#replykeyboardmarkup
                :param inline_keyboard: refer InlineKeyboardMarkup type
            https://core.telegram.org/bots/api#inlinekeyboardmarkup
                :return: Response object from requests module
            http://docs.python-requests.org/en/master/api/#requests.Response
        """
        data = {}
        data["chat_id"] = self.chat_id
        data["text"] = reply_text
        if keyboard:
            data["reply_markup"] = {"keyboard": keyboard, "one_time_keyboard": True}
        elif inline_keyboard:
            data["reply_markup"] = {"inline_keyboard": inline_keyboard}
        r = requests.post(get_url("sendMessage"), json=data)
        return r

    def __repr__(self):
        return "Message: {} - {}".format(self.text, self.name_or_username())
