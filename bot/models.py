from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from .choices import timezones as tzchoices


class Profile(models.Model):
    """ Extension of the user model """

    # https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telegram_id = models.IntegerField(null=True, default=None)
    username = models.CharField(max_length=64, blank=True)
    first_name = models.CharField(max_length=64, blank=True)
    last_name = models.CharField(max_length=64, blank=True)
    state = models.CharField(max_length=64, default="start")
    created_at = models.DateTimeField(auto_now_add=True)
    timezone = models.CharField(
        max_length=64, default="Asia/Kuala_Lumpur", choices=tzchoices
    )

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Habit(models.Model):
    """ Reminder """

    user = models.ForeignKey("Profile", on_delete=models.CASCADE)
    name = models.CharField(max_length=64, verbose_name="Habit name")
    time = models.TimeField(verbose_name="Time (Format: HH:MM:SS)")

    def __str__(self):
        return "{} {}".format(self.name, self.time)


class Reply(models.Model):
    """ Replies from each reminder triggered """

    user = models.ForeignKey("Profile", on_delete=models.CASCADE)
    habit = models.ForeignKey("Habit", on_delete=models.CASCADE)
    note = models.CharField(max_length=64, blank=True)
    done = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Replies"
