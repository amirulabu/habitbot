from .callback import Callback


def process_buttons(update):
    callback = Callback(update)
    operation = callback.habit_reply_operation

    if operation == "replyhabit":
        callback.save_habit_reply()
        callback.remove_inline_keyboard()
        if callback.habit_reply_addnote == "addnote":
            callback.ask_reply_note()
