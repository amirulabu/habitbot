import re
import datetime
import time as libtime
import pytz
import requests

from habitbot.settings import TELEGRAM_BOT_TOKEN
from .models import Habit, Profile


def get_url(method):
    return "https://api.telegram.org/bot{}/{}".format(TELEGRAM_BOT_TOKEN, method)


# logging


def check_time_format(text):
    # can scan these formats:
    # reading book 8.00pm
    # reading book 2000hrs
    # reading book 8:00pm
    # reading book 08.00am
    # reading book 08:00pm
    return re.search(r"^\d(\d|\.|\d\.|\:|\d\:)?(\d\d)?(hrs|am|pm)$", text)


def fix_time_format(text):
    hour, minute = "", ""
    if re.search(r"^\d(\d\.|\.)\d\d(am)", text):
        hour = re.match(r"(^\d(\d|))", text)[0]
        # cater for 12.00am
        hour = "00" if hour == "12" else hour
        hour = "0{}".format(hour) if len(hour) == 1 else hour
        minute = re.search(r"(?P<minute>\d\d)am", text).group("minute")

    elif re.search(r"^\d(\d\.|\.)\d\d(pm)", text):
        hour = str(int(re.match(r"(^\d(\d|))", text)[0]) + 12)
        # cater for 12.00pm
        hour = "12" if hour == "24" else hour
        minute = re.search(r"(?P<minute>\d\d)pm", text).group("minute")

    elif re.search(r"^\d?\d(am)", text):
        hour = str(int(re.match(r"^\d?\d", text)[0]))
        hour = "00" if hour == "12" else hour
        minute = "00"

    elif re.search(r"^\d?\d(pm)", text):
        hour = str(int(re.match(r"^\d?\d", text)[0]) + 12)
        hour = "12" if hour == "24" else hour
        minute = "00"

    else:
        hour = re.match(r"^\d\d", text)[0]
        minute = re.search(r"(?P<minute>\d\d)hrs", text).group("minute")
    # It must be in HH:MM[:ss[.uuuuuu]] format."]
    return str(hour) + ":" + str(minute)


def reset_all_profile_state():
    # reset all profile.state to "start"
    profiles = Profile.objects.exclude(state="start")
    for profile in profiles:
        return_dict = {"chat_id": profile.telegram_id, "text": "No notes saved"}
        r = requests.post(get_url("sendMessage"), json=return_dict)
        profile.state = "start"
        profile.save()
    return profiles.count()


def check_and_trigger_habit():
    context = []
    # get all habits in db
    habits = Habit.objects.all()
    # get time in utc
    time_utc = datetime.datetime.now(datetime.timezone.utc)
    if habits:
        for habit in habits:
            # get user's timezone
            user_timezone = pytz.timezone(habit.user.timezone)
            # get time in user's timezone
            time_user = time_utc.astimezone(user_timezone)
            # get time to compare, now-1min and now+59min
            # if using time_user_now without minus 1 min only it will not trigger precisely 00 minutes, 00 seconds
            time_user_now = (time_user - datetime.timedelta(minutes=1)).time()
            time_user_59min = (time_user + datetime.timedelta(minutes=59)).time()
            time_2300hrs = datetime.time(hour=23, minute=0, second=0)
            time_2359hrs = datetime.time(hour=23, minute=59, second=0)
            time_0000hrs = datetime.time(hour=0, minute=0, second=0)
            inline_keyboard_arr = [
                [
                    {
                        "text": "✅ Done",
                        "callback_data": "replyhabit-{}-done".format(habit.id),
                    },
                    {
                        "text": "❌ Incomplete",
                        "callback_data": "replyhabit-{}-incomplete".format(habit.id),
                    },
                ],
                [
                    {
                        "text": "✅ Done & 📝 add note",
                        "callback_data": "replyhabit-{}-done-addnote".format(habit.id),
                    },
                    {
                        "text": "❌ Incomplete & 📝 add note",
                        "callback_data": "replyhabit-{}-incomplete-addnote".format(
                            habit.id
                        ),
                    },
                ],
            ]

            if time_user_now <= habit.time <= time_user_59min:
                return_dict = {
                    "chat_id": habit.user.telegram_id,
                    "text": "{} {}".format(habit.name, habit.time),
                    "reply_markup": {"inline_keyboard": inline_keyboard_arr},
                }
                r = requests.post(get_url("sendMessage"), json=return_dict)
                print(r)
                context.append(return_dict)
            # when current time in user's timezone is between 2300hrs to 2359hrs
            elif time_2300hrs <= time_user_now <= time_2359hrs:
                # if habit time is between time_user_now and 2359hrs
                # OR habit time is between 0000hrs and time_user_now+59min
                if (time_user_now <= habit.time <= time_2359hrs) or (
                    time_0000hrs <= habit.time <= time_user_59min
                ):
                    return_dict = {
                        "chat_id": habit.user.telegram_id,
                        "text": "{} {}".format(habit.name, habit.time),
                        "reply_markup": {"inline_keyboard": inline_keyboard_arr},
                    }
                    r = requests.post(get_url("sendMessage"), json=return_dict)
                    print(r)
                    context.append(return_dict)
    return context
