import datetime
import time as libtime
import pytz
import random

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.urls import reverse

from habitbot.settings import CURRENT_DOMAIN
from .msg import Message
from .models import Profile, Habit, Reply
from .helper import check_time_format, fix_time_format, get_url
from .choices import timezones

HELP_TEXT = """/start and /help shows this text
/add <habit> <time> to add a habit
/list to list all saved habits
/remove <id> to remove habits
/editname <id> <new name>
/edittime <id> <new time>
/settimezone <TZ database name> 
/resetpassword to get username and link to reset password

Reminder will trigger hourly and rounded to the current hour eg. 
if you set at 9.10am, it will remind you at 9.00am
if you set at 10.50am, it will remind you at 10.00am
"""

ADD_TEXT = """Usage: /add <habit> <time>
Eg. /add reading book 8.00pm
Eg. /add reading book 2000hrs

Habit will be triggered in 30 minutes interval. 
Eg. 9.00am will trigger at 9.00am
Eg. 9.20am will trigger at 9.00am
Eg. 9.30am will trigger at 9.30am
Eg. 9.50am will trigger at 9.30am
"""

REMOVE_TEXT = """Usage: /remove <id>
To get habit's id, /list
"""

EDIT_TEXT = """/editname <id> <new name>
/edittime <id> <new time>
To get habit's id, /list
"""

TIMEZONE_TEXT = """/settimezone <TZ database name> 
Check here https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
Case sensitive!
"""


def process_message(update):
    message = Message(update)
    command = message.text.lower().split(" ")[0]
    try:
        profile = Profile.objects.get(telegram_id=message.chat_id)
        state = profile.state
    except:
        print("Profile not done yet")

    if command == "/start":
        print("start")
        start(message)
    elif command == "/help":
        help_(message)
    elif command == "/add":
        add(message)
    elif command == "/list":
        list_(message)
    elif command == "/remove":
        remove(message)
    elif command == "/editname":
        edit(message, column="name")
    elif command == "/edittime":
        edit(message, column="time")
    elif command == "/settimezone":
        set_UTC_offset(message)
    elif command == "/servertime":
        servertime(message)
    elif command == "/resetpassword":
        reset_password(message)
    elif state != "start":
        # state is <reply_id>-<the state>
        state_arr = state.split("-")
        if state_arr[1] == "note":
            reply_id = int(state_arr[0])
            save_note(message, reply_id)
    else:
        message.reply_message(reply_text=message.text)
        print("text: ", message.text)


def start(message):
    u = Profile.objects.filter(telegram_id=message.chat_id).first()
    reply_text = ""
    if u == None:
        # if user is not in db, then save the details
        user = User.objects.create(
            username="{}{}".format(message.first_name, hash(random.random())),
            password=hash(random.random()),
        )
        profile = Profile.objects.filter(user=user).first()
        profile.telegram_id = "{}".format(message.chat_id)
        profile.username = "{}".format(message.username if message.username else "")
        profile.first_name = "{}".format(message.first_name)
        profile.last_name = "{}".format(message.last_name if message.last_name else "")
        profile.state = "start"
        profile.save()
        reply_text = "Data from {} saved!\n".format(profile.first_name)
    else:
        reply_text = "Welcome {}\nTimezone: {}\n".format(u.first_name, u.timezone)
    message.reply_message(reply_text=reply_text + HELP_TEXT)


def help_(message):
    message.reply_message(reply_text=HELP_TEXT)


def add(message):
    text = message.text
    text_arr = text.split(" ")
    # /add <habit> <time>
    l = len(text_arr)
    p = Profile.objects.filter(telegram_id=message.chat_id).first()
    if p:
        # take words from [1] to second last
        habit = " ".join(text_arr[1 : l - 1])
        # take last word as time
        time = text_arr[l - 1]
        if check_time_format(time):
            # save habit
            h = Habit.objects.create(user=p, name=habit, time=fix_time_format(time))
            message.reply_message(
                reply_text="Saved!\nHabit: {}\nTime: {}".format(habit, time)
            )
        else:
            message.reply_message(reply_text=ADD_TEXT)
    else:
        print("Profile not found")
        message.reply_message(reply_text=ADD_TEXT)


def list_(message):
    reply_text = ""
    p = Profile.objects.filter(telegram_id=message.chat_id).first()
    h = Habit.objects.filter(user=p)
    if h:
        for i in h:
            reply_text = reply_text + "ID: {} - {} - {}\n".format(i.id, i.name, i.time)
    else:
        reply_text = "No habits added yet, use /add <habit> <time>"
    message.reply_message(reply_text=reply_text)


def edit(message, column="name"):
    """ 
    /editname <id> <new name>
    /edittime <id> <new time>
    """
    text_arr = message.text.split(" ")
    l = len(text_arr)
    try:
        p = Profile.objects.filter(telegram_id=message.chat_id).first()
        h = Habit.objects.filter(user=p)
        id_edit = int(text_arr[1])
        habit = h.get(id=id_edit)

        if column == "name":
            habit.name = " ".join(text_arr[2 : l + 1])
            habit.save()
            message.reply_message(
                reply_text="Saved!\nHabit: {}\nTime: {}".format(habit.name, habit.time)
            )
        elif column == "time":
            time = text_arr[2]
            if check_time_format(time):
                habit.time = fix_time_format(time)
                habit.save()
                message.reply_message(
                    reply_text="Saved!\nHabit: {}\nTime: {}".format(
                        habit.name, habit.time
                    )
                )
            else:
                message.reply_message(reply_text=EDIT_TEXT)

    except (KeyError, IndexError):
        message.reply_message(reply_text=EDIT_TEXT)

    except ObjectDoesNotExist:
        message.reply_message(reply_text="ObjectDoesNotExist")
        message.reply_message(reply_text=EDIT_TEXT)


def remove(message):
    text = message.text
    text_arr = text.split(" ")
    l = len(text_arr)
    try:
        id_remove = int(text_arr[1])
        print(message.chat_id)
        try:
            p = Profile.objects.filter(telegram_id=message.chat_id).first()
            h = Habit.objects.get(id=id_remove)
            # delete using markup
            if h.user.id == p.id:
                Habit.objects.get(id=id_remove).delete()
                message.reply_message(reply_text="Delete {} {}".format(h.name, h.time))
            else:
                message.reply_message(
                    reply_text="Error: Fail to delete ID {}".format(id_remove)
                )
        except ObjectDoesNotExist:
            message.reply_message(
                reply_text="ObjectDoesNotExist - Error: Fail to delete ID {}".format(
                    id_remove
                )
            )
    except (KeyError, IndexError):
        p = Profile.objects.filter(telegram_id=message.chat_id).first()
        h_user = Habit.objects.filter(user=p)
        if h_user:
            arr = []
            for i in h_user:
                arr_1 = []
                arr_1.append("/remove {} - {} - {}".format(i.id, i.name, i.time))
                arr.append(arr_1)
            arr.append(["Cancel"])
            print(arr)
        message.reply_message(reply_text=REMOVE_TEXT, keyboard=arr)


def set_UTC_offset(message):
    # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
    text = message.text
    text_arr = text.split(" ")
    try:
        input_tz = text_arr[1]
        u = Profile.objects.filter(telegram_id=message.chat_id).first()
        saved_tz = False
        for timezone in timezones:
            if input_tz in timezone:
                u.timezone = input_tz
                u.save()
                message.reply_message(reply_text="timezone {} saved!".format(input_tz))
                saved_tz = True
                break

        if saved_tz == False:
            message.reply_message(
                reply_text="timezone {} not saved. \nCheck here https://en.wikipedia.org/wiki/List_of_tz_database_time_zones \n Case sensitive!".format(
                    input_tz
                )
            )
    except:
        message.reply_message(reply_text=TIMEZONE_TEXT)


def servertime(message):
    message.reply_message(
        reply_text="{} {}".format(
            datetime.datetime.now().strftime("%H%M"), libtime.tzname
        )
    )


def reset_password(message):
    try:
        p = Profile.objects.filter(telegram_id=message.chat_id).first()
        user = p.user
        # give link with token to user
        # {{ protocol }}://{{ domain }}{% url 'password_reset_confirm' uidb64=uid token=token %}
        domain = CURRENT_DOMAIN
        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
        token = default_token_generator.make_token(user)
        reset_link = reverse(
            "password_reset_confirm", kwargs={"uidb64": uid, "token": token}
        )
        message.reply_message(
            reply_text="Reset password link: https://{}{}".format(domain, reset_link)
        )
        message.reply_message(reply_text="Your username below")
        message.reply_message(reply_text="{}".format(user.username))
    except:
        message.reply_message(reply_text="user not found")


def save_note(message, reply_id):
    reply = Reply.objects.get(id=reply_id)
    reply.note = message.text
    reply.save()
    message.reply_message(
        reply_text='Note for "{}" successfully saved'.format(reply.habit.name)
    )
    # reset profile state
    reply.user.state = "start"

