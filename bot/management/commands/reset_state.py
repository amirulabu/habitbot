import requests
import datetime

from django.core.management.base import BaseCommand
from bot.helper import check_and_trigger_habit, get_url, reset_all_profile_state
from habitbot.settings import TELEGRAM_BOT_TOKEN, TELEGRAM_USER_ID


class Command(BaseCommand):
    help = "Reset state for all profiles which state is not 'start'"

    def handle(self, *args, **options):
        profiles_reset_count = reset_all_profile_state()

        if TELEGRAM_DEBUG == "yes":
            return_dict = {
                "chat_id": TELEGRAM_USER_ID,
                "text": "Reset state completed\ntotal profiles reset: {}".format(
                    profiles_reset_count
                ),
                "disable_notification": True,
            }
            r = requests.post(get_url("sendMessage"), json=return_dict)
            print(r)
