import requests

from django.core.management.base import BaseCommand
from bot.helper import get_url
from habitbot.settings import TELEGRAM_BOT_TOKEN, TELEGRAM_USER_ID


class Command(BaseCommand):
    help = "Set webhook to telegram bot"

    def add_arguments(self, parser):
        parser.add_argument("url")

    def handle(self, *args, **options):
        if TELEGRAM_BOT_TOKEN in options["url"]:
            url = options["url"]
        else:
            url = "%s/bot/%s/" % (options["url"], TELEGRAM_BOT_TOKEN)
            
        setwebhook = requests.get(get_url("setWebhook"), data={"url": url})
        r = requests.get(get_url("getWebhookInfo"))
        if r.json()["result"]["url"]:
            self.stdout.write(
                self.style.SUCCESS('Successfully set webhook to "%s"' % url)
            )
            r = requests.post(
                get_url("sendMessage"),
                json={
                    "chat_id": TELEGRAM_USER_ID,
                    "text": "Successfully set webhook to {}".format(url),
                },
            )
        else:
            self.stdout.write(
                self.style.NOTICE(
                    "Webhook not set. Status code: %s" % setwebhook.status_code
                )
            )

