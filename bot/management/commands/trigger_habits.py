import requests
import datetime

from django.core.management.base import BaseCommand
from bot.helper import check_and_trigger_habit, get_url, reset_all_profile_state
from habitbot.settings import TELEGRAM_BOT_TOKEN, TELEGRAM_USER_ID, TELEGRAM_DEBUG


class Command(BaseCommand):
    help = "Trigger habits hourly according to the profile's timezone"

    def handle(self, *args, **options):
        context = check_and_trigger_habit()
        print(context)
        if TELEGRAM_DEBUG == "yes":
            time_utc = datetime.datetime.now(datetime.timezone.utc)
            return_dict = {
                "chat_id": TELEGRAM_USER_ID,
                "text": "context: {} \ntime_utc: {}".format(context, time_utc),
                "disable_notification": True,
            }
            r = requests.post(get_url("sendMessage"), json=return_dict)
            print(r)
