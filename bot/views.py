import requests
import json
import datetime
import calendar

from django.shortcuts import render, get_object_or_404, reverse, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.db.models import Prefetch

from .commands import process_message
from .buttons import process_buttons
from .models import Profile, Habit, Reply
from .callback import Callback
from .forms import ReplyForm, HabitForm


@csrf_exempt
def bot_index(request):
    if request.method == "POST":
        update = json.loads(request.body.decode("utf-8"))
        print("Update: ", update)
        if "message" in update:
            process_message(update)
        elif "callback_query" in update:
            process_buttons(update)
        return HttpResponse("ok")
    elif request.method == "GET":
        return HttpResponse(status=404)


def trigger_habit(request):
    return HttpResponse("triggered")


@login_required
def index(request):
    user = request.user
    profile = get_object_or_404(Profile, user=user)
    habits = Habit.objects.prefetch_related(Prefetch("reply_set")).filter(user=profile)
    date_now = datetime.datetime.now()
    month = date_now.month
    year = date_now.year
    dates = [date for date in list(range(1, calendar.monthrange(year, month)[1] + 1))]
    return render(
        request,
        "bot/index.html",
        context={"habits": habits, "dates": dates, "date_now": date_now},
    )


@login_required
def habit(request, habit_id):
    user = request.user
    profile = get_object_or_404(Profile, user=user)
    habit = Habit.objects.get(id=habit_id)
    replies = Reply.objects.filter(habit=habit).order_by("-created_at")
    total_done = replies.filter(done=True).count()
    total_incomplete = replies.filter(done=False).count()
    return render(
        request,
        "bot/habit.html",
        context={
            "habit": habit,
            "replies": replies,
            "total_done": total_done,
            "total_incomplete": total_incomplete,
        },
    )


@login_required
def edit_reply(request, reply_id):
    reply = Reply.objects.get(id=reply_id)
    date = reply.created_at
    if reply.user.user != request.user:
        return HttpResponse(status=404)

    if request.method == "POST":
        form = ReplyForm(request.POST)
        if form.is_valid():
            reply.note = form.cleaned_data["note"]
            reply.done = form.cleaned_data["done"]
            reply.save()
            return HttpResponseRedirect(reverse("habit", args=[reply.habit.id]))
        else:
            messages.error(request, form.errors)
            return HttpResponseRedirect(reverse("edit_reply", args=[reply_id]))
    else:
        form = ReplyForm(instance=reply)
    return render(
        request, "bot/edit_reply.html", {"form": form, "reply": reply, "date": date}
    )


@login_required
def add_habit(request):
    user = request.user
    profile = get_object_or_404(Profile, user=user)
    if request.method == "POST":
        form = HabitForm(request.POST)
        if form.is_valid():
            habit = form.save(commit=False)
            habit.user = profile
            habit.save()
            return HttpResponseRedirect(reverse("habit", args=[habit.id]))
    else:
        form = HabitForm()
        return render(request, "bot/add_habit.html", {"form": form})
