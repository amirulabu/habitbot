from django.urls import path
from habitbot.settings import TELEGRAM_BOT_TOKEN

from . import views
from django.contrib.auth.views import PasswordResetConfirmView, PasswordResetCompleteView

urlpatterns = [
    path("bot/{}/".format(TELEGRAM_BOT_TOKEN), views.bot_index),
    path("bot/{}/triggerHabit".format(TELEGRAM_BOT_TOKEN), views.trigger_habit),
    path("", views.index, name='index'),
    path("<int:habit_id>/", views.habit, name='habit'),
    path("add-habit/", views.add_habit, name="add_habit"),
    path("reply/<int:reply_id>/edit", views.edit_reply, name="edit_habit"),
]
