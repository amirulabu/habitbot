import requests
from .helper import get_url
from .models import Habit, Reply, Profile


class Callback:
    """ 
    Class to manage callback query from user when inline keyboard button is triggered
    https://core.telegram.org/bots/api#callbackquery
    """

    def __init__(self, update):
        self.update = update
        self.message = update["callback_query"]["message"]
        self.text = update["callback_query"]["message"]["text"]
        self.chat_id = update["callback_query"]["from"]["id"]
        self.message_id = update["callback_query"]["message"]["message_id"]
        self.first_name = update["callback_query"]["from"]["first_name"]
        # Telegram allow accounts without last name
        self.last_name = (
            update["callback_query"]["from"]["last_name"]
            if update["callback_query"]["from"].get("last_name")
            else None
        )
        # Telegram allow accounts without username
        self.username = (
            update["callback_query"]["from"]["username"]
            if update["callback_query"]["from"].get("username")
            else None
        )
        self.is_bot = update["callback_query"]["from"]["is_bot"]
        self.date = update["callback_query"]["message"]["date"]
        data_arr = update["callback_query"]["data"].split("-")
        # data arr format is <operation>-<id>-<reply>-<add note or not>
        self.habit_reply_operation = data_arr[0]
        if self.habit_reply_operation == "replyhabit":
            self.habit_reply_id = data_arr[1]
            self.habit_reply_done = data_arr[2]
            self.habit_reply_addnote = data_arr[3] if len(data_arr) == 4 else None
        self.reply_id = None

    def save_habit_reply(self):
        habit = Habit.objects.get(id=self.habit_reply_id)
        profile = Profile.objects.get(telegram_id=self.chat_id)
        done = False
        if self.habit_reply_done == "done":
            done = True
        reply = Reply.objects.create(user=profile, habit=habit, note="", done=done)
        self.reply_id = reply.id
        return self.habit_reply_done

    def remove_inline_keyboard(self):
        text_done = "(✅ Done)" if self.habit_reply_done == "done" else "(❌ Incomplete)"
        return_dict = {
            "chat_id": self.chat_id,
            "message_id": self.message_id,
            "text": "{} - {}".format(self.text, text_done),
        }
        r = requests.post(get_url("editMessageText"), json=return_dict)
        print(r)

    def ask_reply_note(self):
        profile = Profile.objects.get(telegram_id=self.chat_id)
        reply = Reply.objects.get(id=self.reply_id)
        profile.state = "{}-note".format(self.reply_id) if self.reply_id else "error"
        profile.save()
        return_dict = {
            "chat_id": self.chat_id,
            "text": 'Add some notes about "{}"? If not, just ignore me'.format(
                reply.habit.name
            ),
        }
        r = requests.post(get_url("sendMessage"), json=return_dict)
        print(r)
