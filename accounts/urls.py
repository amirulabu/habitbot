from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from .forms import AuthenticationFormExtra, PasswordResetFormExtra, SetPasswordFormExtra

urlpatterns = [
    path("", views.profile, name="profile"),
    path("edit-profile/", views.edit_profile, name="edit_profile"),
    path(
        "login/",
        auth_views.LoginView.as_view(
            template_name="accounts/login.html",
            authentication_form=AuthenticationFormExtra,
        ),
        name="login",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("password-change/", views.change_password, name="password_change"),
    path(
        "password-reset/",
        auth_views.PasswordResetView.as_view(
            template_name="accounts/reset.html", form_class=PasswordResetFormExtra
        ),
        name="password_reset",
    ),
    path(
        "password-reset/done/",
        auth_views.PasswordResetView.as_view(
            template_name="accounts/reset.html",
            form_class=PasswordResetFormExtra,
            extra_context={"alert_success": "Password reset successful"},
        ),
        name="password_reset_done",
    ),
    path(
        "password-reset/<uidb64>/<token>",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="accounts/password_change.html",
            form_class=SetPasswordFormExtra,
        ),
        name="password_reset_confirm",
    ),
    path(
        "password-reset/login",
        auth_views.LoginView.as_view(
            template_name="accounts/login.html",
            authentication_form=AuthenticationFormExtra,
            extra_context={
                "alert_success": "Password reset completed, please login with your new password"
            },
        ),
        name="password_reset_complete",
    ),
]

