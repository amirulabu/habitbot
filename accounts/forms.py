from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import (
    SetPasswordForm,
    AuthenticationForm,
    PasswordResetForm,
)
from bot.models import Profile
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Button, Submit, HTML, Field, Div


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username"]

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Submit"))
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-2"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(Field("username"))


class SetPasswordFormExtra(SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super(SetPasswordFormExtra, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Submit"))
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-2"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Fieldset("Set new password", "new_password1", "new_password2")
        )


class AuthenticationFormExtra(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(AuthenticationFormExtra, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Submit"))
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-md-2"
        self.helper.field_class = "col-md-8"
        self.helper.layout = Layout(Fieldset("Login", "username", "password"))


class PasswordResetFormExtra(PasswordResetForm):
    def __init__(self, *args, **kw):
        super(PasswordResetFormExtra, self).__init__(*args, **kw)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-2"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Fieldset(
                "Reset Password",
                "email",
                Div(
                    Submit("submit", "Reset password", css_class="btn btn-default"),
                    HTML('<a class="btn btn-default" href="/">Cancel</a>'),
                    css_class="text-center",
                ),
            )
        )

