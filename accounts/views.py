from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import logout, login, authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User

from bot.models import Profile
from .forms import UserForm, SetPasswordFormExtra, AuthenticationFormExtra


@login_required
def profile(request):
    profile = Profile.objects.get(user=request.user)
    return render(request, "accounts/profile.html", context={"profile": profile})


@login_required
def change_password(request):
    if request.method == "POST":
        form = SetPasswordFormExtra(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, "Your password was successfully updated!")
            return redirect("index")
        else:
            messages.error(request, "Please correct the error below.")
    else:
        form = SetPasswordFormExtra(request.user)
    return render(request, "accounts/password_change.html", context={"form": form})


@login_required
def edit_profile(request):
    user = User.objects.get(id=request.user.id)
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user.username = form.cleaned_data['username']
            user.save()
            return redirect("profile")
        else:
            messages.error(request, form.errors)
            return redirect("edit_profile")
    else:
        form = UserForm()
    return render(request, "accounts/edit_profile.html", context={"form": form})
